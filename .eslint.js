module.exports = {
  extends: ["airbnb-base", "prettier"],
  env: {
    browser: true,
  },
  rules: {
    "no-console": 0,
    "no-restricted-syntax": 0,
    "no-unused-expressions": "off",
  },
  overrides: [
    {
      files: ["__tests__/**/*.js", "**/*.test.js"],
      env: {
        jest: true,
      },
      rules: {
        "import/no-extraneous-dependencies": 0,
        "no-param-reassign": 0,
      },
    },
  ],
};
