#!/usr/bin/env node
const path = require("path");

const webpackConfigPath = path.resolve(__dirname, "webpack.config.js");
const pathSeparator = process.platform === "win32" ? ";" : ":";

const commandConfig = {
  dev: `webpack serve -c ${webpackConfigPath}`,
  build: `webpack -c ${webpackConfigPath}`,
  verify: `lint-staged -c ${path.resolve(
    __dirname
  )}/package.json && jsninja-sensei`,
};

const runCommand = (command, args) => {
  // eslint-disable-next-line global-require
  const cp = require("child_process");

  return new Promise((resolve, reject) => {
    const executedCommand = cp.spawn(command, args, {
      stdio: "inherit",
      shell: true,
      env: {
        PATH: `${path.resolve(
          __dirname,
          "./node_modules/.bin"
        )}${pathSeparator}${process.env.PATH}`,
      },
    });

    executedCommand.on("error", (error) => {
      reject(error);
    });

    executedCommand.on("exit", (code) => {
      if (code === 0) {
        resolve();
      } else {
        reject();
      }
    });
  });
};

function start() {
  const { 2: command } = process.argv;
  const scriptToRun = commandConfig[command];
  runCommand(scriptToRun).catch((error) => console.log(error));
}

start();
