#!/usr/bin/env node
const fs = require("fs");

const getParentConfigPath = (filename) =>
  `@jsninja/basic-course-scripts-cypress/${filename}`;

const configsForGenerating = [
  {
    filename: ".eslintrc.js",
    fileContent: `module.exports = {
    extends: require.resolve("${getParentConfigPath(".eslint")}"),
  };`,
  },
  {
    filename: "prettier.config.js",
    fileContent: `module.exports = require("${getParentConfigPath(
      "prettier"
    )}");
  `,
  },
];

function createConfigFile(fileInfo) {
  const filePath = `${process.env.INIT_CWD}/${fileInfo.filename}`;
  if (fs.existsSync(filePath)) {
    return;
  }

  fs.writeFile(filePath, fileInfo.fileContent, (err) => {
    if (err) throw err;
  });
}

configsForGenerating.map(createConfigFile);
